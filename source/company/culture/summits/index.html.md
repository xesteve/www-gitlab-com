---
layout: markdown_page
title: "GitLab Summits"
---

- TOC
{:toc}
- [Summit From a Leadership Perspective](/company/culture/summits/summit-training)


## What, where, and when
{: #what-when-where}

We try to get together every 9 months or so to get face-time with one another, build community,
and get some work done! Since our team is scattered all over the globe, we try to
plan a different location for each Summit.

The summit is not a mandatory trip or a department offsite, nor is it a vacation or incentive trip. It's a chance for everyone to meet fellow GitLabbers across all departments and regions: part team building, part education, part customer interaction, and hopefully all fun.

## Goal

The goal of our Summits is to **get to know the people in the GitLab community better**.
The better you know people, the easier it is to collaborate.
We want to build trust between groups.

## Social media

You are encouraged to take pictures and post on social media.
If you take pictures of people not on the podium please ask their permission.
Please consider using the hashtag #GitLabSummit
Our [social media guidelines](/handbook/marketing/social-media-guidelines/) still apply.

## Who
{: #who}

All team members of the [GitLab company](/company/team/), SOs, [Core Team](/core-team/), contributors, customers, press, and anyone interested in joining.

## Summits

### Upcoming Summit

The next summit coming up will be in May 2019 in New Orleans, Louisiana in the United States!
All info to prepare for this will be on the [project page](https://gitlab.com/gitlabcontribute/new-orleans) as soon as we've signed with a hotel


#### Countdown

<p class="alert alert-gitlab-purple text-center">
<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i>
Counting down to May 2019!
<i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i>
<br/><br/>
<span class="h3" id="nextSummitCountdown">
<i class="fas fa-spinner fa-spin fa-fw" style="color:rgb(107,79,187); font-size:.85em" aria-hidden="true"></i>
</span>
</p>

{: #previous-summits}

### Summit in Cape Town, South Africa

Last August, in 2018, our team had grown to about 320 people. Over 260 of them came to Cape Town, South Africa. We had over 85 SOs present and were also joined by a handful of customers, our advisors, and investors.

![GitLab Summit - South Africa - 2018](/images/gitlab-summit-south-africa.jpg){: .illustration}*<small>In August 2018, the whole team had grown to 320 GitLabbers!</small>*

### Summit in Crete, Greece

By October 2017 we had 200 team members and 65 significant others getting together in Greece to enjoy the beautiful islands of Crete and Santorini.

![The GitLab Team in October 2017](/images/gitlab_team_summit_greece_2017.png){: .illustration}*<small>When October 2017 came around, the whole team already counted 200 GitLabbers!</small>*


### Summit in Cancun, Mexico

In January 2017, we met in Cancun, Mexico, where roughly 150 team members and 50
significant others flew in from 35 different countries.

![The GitLab team in January 2017](/images/team-photo-mexico-summit.jpg){: .illustration}*<small>In January 2017, the whole team had grown to 150 GitLabbers!</small>*

<figure class="video_container">
 <iframe src="https://www.youtube.com/embed/XDfTj8iv9qw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<br>

### Summit in Austin, Texas

In May 2016, our team of 85 met up in Austin, TX to see if they were (still) as
awesome as they seemed on Google Hangout.

Here's some footage our team put together to show how much fun we had:

![The GitLab team in May 2016](/images/resize1200-team-austin.jpeg){: .illustration}*<small>Back in May 2016, the whole team was a total of 85 GitLabbers</small>*

<figure class="video_container">
 <iframe src="https://player.vimeo.com/video/175270564" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<br>

### Summit in Amsterdam, the Netherlands

Here are some impressions from our [second summit](/2015/11/30/gitlab-summit-2015/) in October 2015.

<figure class="video_container">
 <iframe src="https://www.youtube.com/embed/GJP-3BNyCXw" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<br>

### Summit in Novi Sad, Serbia

First summit, less then 10 people attending; highlight was lunch at Marin's mom's home in October 2013.

![GitLab Summit - Serbia - 2013](/images/gitlab-summit-novi-sad.png)

<br>

## Leisure time around the Summit
* The GitLab Summit is a work trip, not an incentive trip
* If you want to enjoy the resort facilities or the area around it, feel free to book an extra day or more _before or after_ the summit
* The Summit organization will plan "regular work time" for you to do regular work such as handling emails
* When you sign up for the activities we plan for the non-work days, you agree to show up.
* If you don't show up for the activity, you will be responsible for the costs involved for the seat you give up after the RSVP deadline has passed.
* When you replied "Maybe" or didn't reply at all, you understand that there will not be a ticket booked for you and you won't be able to join the activitie(s).

## Bring your significant other

* Significant Others (SO) are very welcome to attend
* One SO per team member
* You are responsible for the SO you invite
* Your SOs presence should not distract you from engaging with other team members and actively participating in Summit activities
* SOs should do their best to get to know many people
* SOs are welcome at all presentations, events, and meetings that are open to
all team members
* If you're having a meal with your SO, pick a table with more than two seats
so you can invite others to join you

## Having your children join you

Children are strongly discouraged from attending. We have observed from past summits
that contributors who have chosen to bring children spend significantly less time
collaborating with GitLab coworkers. Accordingly, we are requesting that if you
must travel with family members you fully engage in as many group activities as
you are physically able, invite team members to join you during meals, attend all
company meetings, and recognize that the Summit is an investment in the continued
growth of the organization.

## Leaders at the Summit
{: #leaders}

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/39chczWRKws?start=1756" frameborder="0" allowfullscreen="true"> </iframe>
</figure>*<small>For the context of this video, please see the [summit challenges in the Greece recap](https://about.gitlab.com/2017/10/25/gitlab-summit-greece-recap/#summit-challenges)</small>*

[Company leaders](/handbook/leadership/) who attend the summit should take the opportunity to engage with GitLabbers across the organization and should be mindful of silos and favoritism as they observe team dynamics.

### Be conscious

Leaders should consider themselves "hosts" of the event, which means that, in addition to facilitating team building and interaction, they should be aware of potential issues that could arise during the event related to the behavior, health, and safety guidelines below. These include:
- Harassment, sexual or otherwise
- Excessive drinking and/or drug use
- Fighting and physical altercations
- Accidents (e.g. trips, falls, motor accidents, etc.)
- Inappropriate dress (business/work event)
- Various mental states and moods (anxiety, depression, etc); [neurodiversity](https://about.gitlab.com/handbook/values/#diversity)

### What should managers do?
- Protect and reduce risk to all team members and to the company
- Check on your team throughout summit
- Err on the side of caution and over-communication by contacting People Ops immediately if there is an issue or if you think there may be a problem/concern

**Who to contact?**
- Barbie Brewer and Julie Armendariz - onsite at Summit in South Africa
- Jessica Mitchell - available via Slack
- People Operations Team - can be reached via Slack and people ops email address

## Behavior

* Attending the summit is optional but recommended. Most people report it is great to get to know each-other better and the schedule is fun, about 90% of the team is able to attend.
* The executive team is required to attend for the full duration of the summit.
* Wear your name-tag when you're outside your room, including during excursions, meals, and on departure day.
* Try to join different people every time we sit down for a meal
* Try to form a personal bond with team members of other teams.
* The summit is great for informal meetings and brainstorming, like User Generated Content discussions. People already know their team, so try to make UGC sessions cross functional.
* Don't plan meetings and 1-1's with your own team at the summit, we already do these when we're not at the summit. It is OK to organize one dinner with the team.
* Prior to the summit, ensure to communicate to any external stakeholders (i.e. candidates, customers, vendors, etc) that response time may be less reliable, as you will be out of the office and will not have as much access to email and calls.
* Respect the laws and customs of the location we are visiting.

### Extraverts and Introverts
Remember that you and your co-workers may have different personality types in terms of how you interact with others in large social situations. Consider the differences between extraverts and introverts:

**Extraverts**

- Recharge by being Social
- Enjoy group conversations
- Speak more
- Make decisions quickly
- Love getting attention
- Speak up in meetings

**Introverts**

- Recharge by spending time alone
- Enjoy one-on-one conversations
- Listen more
- Reflect before making decisions
- Are not interested in getting attention
- Shares ideas when prompted

**Ambiverts**

- A combination of both introverts and extraverts

References:
[Are Extraverts Happier Than Introverts? Psychology Today](https://www.psychologytoday.com/blog/thrive/201205/are-extroverts-happier-introverts)
[Are You an Extravert, Introvert, or Ambivert?](https://www.psychologytoday.com/us/blog/cutting-edge-leadership/201711/are-you-extravert-introvert-or-ambivert)

## Health and safety
{: #health-and-safety}

* Look after your self during the summit and avoid summit burnout. It can be an exciting time with lots of new people to meet and things going on you want to take part in. Remember to take down-time if you need it to recuperate during the week rather than trying to [burn the candle at both ends](https://dictionary.cambridge.org/dictionary/english/burn-the-candle-at-both-ends) and risking exhaustion.
* Every year about a third of us have some kind of flu after the summit, so please take infection prevention seriously.
* Use fist-bumps instead of handshaking to [reduce the number of people who get sick](https://www.health.harvard.edu/blog/fist-bump-better-handshake-cleanliness-201407297305).
* Use hand sanitizer after getting your food and before eating. [Shared buffet utensils spread disease.](http://www.cruisereport.com/crBlogDetail.aspx?id=3683) We will try to provide hand sanitizer.
* If you are sick please wear a [surgical mask](https://www.amazon.com/Maryger-Disposable-Procedure-Surgical-Counts/dp/B06XVMT3ZH/ref=sr_1_1_sspa?s=hpc&ie=UTF8&qid=1509481716&sr=1-1-spons&keywords=surgical+mask&psc=1) which [reduces the spreading by up to 80%](https://www.healthline.com/health/cold-flu/mask). We'll try to provide them.
* Remember our [values](/handbook/values/) and specifically the [permission to play](/handbook/values/#permission-to-play) behavior
* Be respectful of other hotel guests (e.g. don't talk on your floor when returning
to your room at night & keep your volume down at restaurants/bars).
* Utilize the resources available to understand the safety and crime considerations in the location we are visiting. Examples are the [UK's Foreign Travel Site](https://www.gov.uk/foreign-travel-advice) and the [U.S. State Department](https://travel.state.gov/content/passports/en/country.html). If you are alarmed by what you are reading, please feel free to reach out to People Ops Team with your concerns. We also advise reviewing the data for countries you feel are safe. You may find that even the safest countries have warnings on crime and safety. Staying with a group and away from known dangerous areas is the most basic way of avoiding problems.

## Cultural Impacts and Differences
It is important to recognize that people from different cultures have different ways of saying things, different body language, different ways of dressing/attire and even different ways of looking at things. You can review examples of typical cultural differences on the [Center of Intercultural Competence](http://www.cicb.net/en/home/examples).

Summit attendees should also remember:
- We have various generations and 40+ countries/cultures coming together for 5 straight days, versus our normal routine of interacting remotely.
- We are guests of this country, of the hotel, and of the sites we will visit - it is our privilege to be there, and we need to be ambassadors of GitLab and lead by example the whole time according to [our values](https://about.gitlab.com/handbook/values/).
- Be sensitive to political, religious and other potentially divisive conversations

### Lost in Translation
Translation tends to sound easier than it is. People often think that it is just a matter of replacing each source word with a corresponding translated word, and then you are done. Assume best intent from your fellow team members and use it as an opportunity start a new dialogue. For more information you can review the following articles on [LTC Language Solutions](https://ltclanguagesolutions.com/blog/lost-in-translation-translating-cultural-context/)

## WiFi

Great WiFi is essential to the success of the summit.
We can't have everyone in one location and not have excellent internet.

1. Main room needs one access point for every 40 people attending in the main room and management, we have our own equipment but need to buy more.
2. We need two different uplinks from two different providers.
3. We need our own router between the uplinks and the WiFi.
4. We need wired connections to the WiFi access points.
5. We need to have WiFi working before the executive team arrives.
6. We need very reliable fast connections if we are livestreaming during the summit.
7. We need a map of the property with all wiring and access points drawn in 3 months before the summit starts.

## Logistical basics

* Ensure there are large meeting rooms for team members to join work hours and presentations
* Tip: Label your charger, or other belongings, with your name for easy return to owner in case you lose it

## Presentations
{: #presentations}

The following should be assigned and/or arranged a month before the Summit:

* All interviews, presentations and content production
* Who will be presenting and when & where they will be presenting
* Projectors, wireless (non-hand-held) microphones, and any other (audio) needs
* Recording equipment such as stage cam, audience cam, presentation feed etc.
* An audio feed that goes directly from microphone into the recording
* A program and manager for live streaming
* The blog text for the presentation, including relevant materials shared after
the presentation, as well as approval and a plan to publish the blog 24 hours after the presentation is given

## Summit Sessions

GitLab Summits have variety of sessions: UGCs, workshops, presentations, etc. A summit isn’t a conference, its a "meeting of minds”. Its a place to connect and collaborate in an environment where everyone can contribute. We don’t present to our users and customers, they present to us.

### UGCs

UGC stands for “user generated content.” The goal is for a group to come together to produce something collectively. A UGC “user” can be anyone in the GitLab community: GitLabber, significant other, customer, contributors, etc. including “GitLab users” (e.g. someone who uses GitLab).

<!-- blank line -->
<figure class="video_container">
 <iframe src="https://www.youtube.com/embed/WmEfRs397B8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### UGC attributes

 1. Everyone can contribute.
  a. Limit to 15 people. (i.e. [Small enough so that everyone can share](https://www.inc.com/jim-schleckser/7-plus-or-minus-2-for-meetings.html))
 2. Content is generated from the discussion, not prepared ahead of time.  3. Each UGC has a google doc to capture notes.
  a. Pro tip: Set up [offline access for Google Drive]() so you can take notes even if wifi is spotty. The doc is then ready to share once you can connect to the internet again.  4. Sharing in real-time is optional.
  a. Everyone _can_ contribute, not everyone _must_ contribute.
  b. Some folks prefer to listen.
  c. Some folks prefer to think about the session then add comments to google doc later.
 5. Can be GitLab or non-GitLab related
  a. GitLab related: produce notes that can be shared. Decided later is any action needs to be taken. Create an MR together on-site.
  b. Non-Gitlab related: Just have a discussion about something you enjoy with other people who enjoy the same thing to build a relationship.
 6. Preparation needed:
  a. A google doc for the session
  b. A facilitator 

#### How to facilitate a UGC

 1. Give a short introduction to the topic (about 2 minutes) for folks that may not be familiar.
 2. Ask some questions to get people talking then let the discuss flow organically. Some example questions:   a. Why did you come to this session?
  b. You mentioned X, can you tell us more about that?
  c. <name>, do you have any thoughts on this topic?
    - Good to ask someone this if they’ve been quiet or haven’t participated yet. Some folks are waiting for an invitation to speak and will appreciate being asked specifically to share. Some folks will say, “no thanks” and prefer to listen. Both are ok.
  d. `<name>`, we’d like to hear what you have to say, but first let’s allow `<name>` finish their thought.
   - Good to interrupt someone who has just interrupted someone else so that everyone can contribute and one person doesn’t dominate the conversation. 

#### UGC results

The following are measures of a successful UGC:
  1. Contribution: the more people contributing, the better. 
 2. Thorough notes: a Google Doc full of notes. There’s no expectation to act on the notes. Once the session is complete, you can decide if there’s a next action.
 3. Relationship building: some UGCs won’t have a next-step action; they were simply opportunities for people who enjoy the same thing to build relationships. Examples might include board games, cooking, or mountain climbing.
 4. Tangible artifacts: assets produced by the group (e.g. an MR, a demo, a diagram, a song, etc.) 

#### UGC coordination

Here's an overview of how this works during our summits.
* We request everyone to [send in topics](https://docs.google.com/spreadsheets/d/15irgt0kOlCNzltsBzWRbJoQzLFjOlOYKgREQ_PRGiuQ/edit#gid=0) to discuss during the sessions at the summit a few weeks out.
  * If you're suggesting a topic, we ask you to be the topic leader. This means the following:
   * You start the session with a short 2 minute verbal introduction, no preparation or presentation needed.
   * During the discussion you facilitate the conversation, meaning keeping it on topic, making sure everyone is heard, and asking relevant questions.
   * When time is up you give a short summary and thank the people that contributed relevant questions and answers.
* After all topics are received, we send a [survey](https://docs.google.com/forms/d/e/1FAIpQLScvxsf00cShgj9KIjgIx8Jj60uVFhE_WbtYbHkoa3d2-H7IxQ/viewform) to all attendees asking them to vote on the topics most interesting to them.
* Once we've received all votes, after the deadline, we select the topics for the sessions.
* Everyone signs up for sessions, we limit every session to 9 people maximum so that everyone can contribute.
* We schedule 2, 4 hour blocks on separate days to have the sessions.
* Within each 4 hour block we schedule 4 session blocks (with multiple topics in different locations during each block) with a short break of 15 minutes in between for a quick drink/snack or bathroom break.

### Workshops

 1. A workshop is interactive training (e.g. Rails Girls, Kubernetes 101, Speaker Training, etc.) 
 2. Everyone can participate. It’s a workshop, not a presentation. Everyone in the room should be able participate.  3. Can be a small or large number of people. Larger groups may need multiple facilitators or coaches to ensure everyone can participate.
 4. Requires preparation and coordination ahead of time to facilitate a productive workshop.
  a. Room needs to be set up.
  b. Outline or the training needs to be created.
  c. Supplemental materials may to be created.
  d. Good WIFI may be required.
 5. Can be simple and lightweight (e.g. bring your laptop and learn: how to use git, how to make an MR, how to contribute to GitLab, etc. This is content that already exists in the handbook/website, but it can be very helpful to have someone walk you through it in person.)

#### Workshop results

The following are measures of a successful workshop.

 1. Equipping: People learn a new skill or improve an existing skill.
 2. Participation: Everyone has the opportunity to participate.

### Presentations

 1. Presentations are one-way, one-to-many communication mode (one person speaking many people listening.)
 2. One-way, one-to-many communication is easy to do remote while UGC-style, many-to-many discussion is harder to do remote.
 3. If a GitLabber has an idea for a presentation they should do this outside of the Summit (e.g. schedule a remote call, livestream or upload the recording to YouTube, link it in the training section of the handbook, etc.)
 4. Customers and Users presentations are good for the Summit so that more GitLabbers can hear from our customers and experience greater customer empathy.

#### Presentation results

The following are measures of a successful presentation.

 1. TODO: Add results

### Team UGCs

Most UGCs can, and should, have diverse group of folks attending like GitLabbers from different teams, SOs, customers, etc. There should also be scheduled time for teams to have team-only UGCs to discuss their work, strategize, and problem solve.

 1. Functional groups meet together (e.g. East Sales, Pipe-to-Spend, Site Reliability Engineering, etc.)
 2. Cross-functional groups meet together (e.g. Plan, Create, Verify, etc.)
 3. Customers meet together without any GitLabbers in the room to talk freely to each other.

## Live stream

1. During the summit we'll have a live stream that is interactive (viewers ask questions).
1. The live stream is a way to generate tangible hiring and marketing benefits, this will help to sustain the large discretionary expense of the summit.
1. There will be only one mobile camera crew and it will be easy to recognize.
1. With the mobile camera crew will be two GitLab team members, a moderator for the chat and a facilitator that will interact with other team members.
1. We want the viewers (our team members that couldn't make it, the wider community, friends and family) to feel like participants instead of an audience. From time to time the facilitator will seek interaction by talking with team members.
1. You can always decline to enter in a conversation with the facilitator, just like you can decline a conversation with another team member.
1. If you don't want to be approached by the facilitator under any circumstances you can ask for an identifier from the organization.
1. The facilitator will avoid taking to team members wearing the identifier, significant others, and children. Significant others that want to interact are very welcome to approach the facilitator themselves.
1. Team members wearing the identifier and significant others might be visible in the shot as passers-by's. The camera crew try to avoid children passers-by's. As said elsewhere on this page we strongly discourage children from attending.

<script>
var nextSummitDate = new Date("May 8, 2019 08:00:00").getTime();

var x = setInterval(function() {
  var now = new Date().getTime();

  var distance = nextSummitDate - now;
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Output the result in an element with id="demo"
  document.getElementById("nextSummitCountdown").innerHTML = days + " days " + hours + " hours "
  + minutes + " minutes " + seconds + " seconds ";

  // If the count down is over, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("nextSummitCountdown").innerHTML = "Already happened!";
  }
}, 1000);
</script>
