---
layout: markdown_page
title: "Agile Delivery with GitLab"
---
## Agile Development and Project Management
Since the publication of the [2001 Agile Manifesto](http://agilemanifesto.org/), development teams have created iterative, incremental and lean approaches to streamline and accelerate the delivery of modern software projects. The techniques have ranged from 'extreme programming' to [Scrum](https://www.scrum.org/) and [Kanban](https://en.wikipedia.org/wiki/Kanban_(development)) where teams are able to rapidly organize, plan and deliver working software. Large enterprises have adopted agile at enterprise scale in many frameworks, ranging from "[Scaled Agile Framework (SAFe)](https://www.scaledagile.com/)" to [Disciplined Agile Delivery](http://www.disciplinedagiledelivery.com/)

GitLab enables teams to apply agile practices and principles to organize and manage their work. Because agile is cool.

## Scrum

[What is Scrum?](https://www.scrum.org/resources/what-is-scrum) Scrum is an agile development framework where teams establish a consistent cadence to organize and deliver value. A scrum team is cross functional development team, which takes their cues from the Product Owner. The Scrum Master is responsible to help protect the team from external distractions. The Scrum team breaks their work into short increments of Sprints ranging from a week to several weeks, where the team focuses on delivering value (working software) that meets the needs of the Product Owner.  

**Scrum Events**

| Scrum Event | Scrum in GitLab |
|------------------------|-----------------|
| Sprint | A sprint represents a finite time period in which the work is to be completed, often 1-3 weeks in duration. The Product Owner and the development team meet to decide work that is in scope for the upcoming sprint. GitLab's milestones feature supports this: assign milestones a start date and a due date to capture the time period of the sprint. The team then pulls issues into that sprint by assigning them to that particular milestone.  |
| Sprint Planning | User stories describe the scope, goals and objectives of new functionality.  User stories also drive an estimation of the technical effort to implement the story. In GitLab, issues are where user stories are captured. Issues have a weight attribute in order to indicate the estimated effort. Cumulative issue weights can be viewed for each column within GitLab issue boards as well as in GitLab milestones. User stories are further broken down to technical deliverables, sometimes documenting technical plans and architecture. In GitLab, this information can be documented in the issue via task lists or in the merge request description, as the merge request is often the place where technical collaboration happens. During the sprint (GitLab milestone), development team members pull user stories to work on one by one. In GitLab, issues have one or more assignees. A GitLab best practice is to create an empty and linked-to-issue merge request right away to start the technical collaboration process, even before creating a single line of code. |
| Daily Scrum |    Throughout the sprint, issues move through various stages, such as Backlog, In progress, Completed and Accepted depending on the workflow in your particular organization. Typically these are columns in an Agile board. In GitLab, issue boards allow you to define your stages, prioritize work in each stage and move issues across the board via drag and drop. The team configures the board with respect to the milestone and other relevant attributes. During daily standups, the team reviews the board together to view the status of the sprint from a workflow perspective. |
| Sprint Review | The development team stays on track in real time and mitigates risks as they arise. GitLab provides burndown charts for milestones, allowing the team to visualize the work scoped in the current sprint "burning down" as it is being completed. Toward the end of the sprint, the development team demos completed features to various stakeholders. With GitLab, this process is made simple using Review Apps so that code can be demoed prior to being pushed to staging or production environments. Review Apps and CI/CD features are integrated with the merge request itself, creating a collaborative hub and a place for peer reviews. These same tools are useful for Developers and QA roles to maintain software quality, whether through automated testing with CI/CD, or manual testing in a Review App environment. |
| Sprint Retrospective | Sprint Retrospectives look back at the previous sprint asking questions such as "what went right," " what could have gone better," and "what will we do different next time?" In GitLab, teams will use the milestone and the issue board to review the previous sprint. Discussions are often added to merge requests for future referencability. Retrospective notes may be entered as a page in the GitLab Wiki. |

**Scrum artifacts**

| Scrum Artifact | Scrum in GitLab |
|------------------------|-----------------|
| Product Backlog | The product or business owners typically create these user stories to reflect the needs of the business and customers. They are prioritized in a product backlog to capture urgency and desired order of development. The product owner communicates with stakeholders to determine the priorities and constantly refines the backlog. In GitLab, dynamically generated issue lists can be viewed to track backlogs. Labels are created and assigned to individual issues, which then enables filtering of issue lists by a single label or multiple labels. Labels can also be prioritized to assist in ordering the issues in those lists. |
| Sprint Backlog | In Agile, you often start with a user story that captures a single feature that delivers business value for users. In GitLab, a single issue within a project serves this purpose. |
| Increment| tbd |



## Kanban
[Kanban](https://en.wikipedia.org/wiki/Kanban_(development)) is a software delivery approach based on the lean manufacturing principles of visualizing work in order to manage the 'work in proceess' (WIP) and reduce non value activities and waste.  The goal of Kanban is to focus on flow of value and allow a team to quickly re-prioritize work based on customer demand.


**Kanban artifacts**

| Kanban Artifact | Kanban in GitLab |
|------------------------|-----------------|
| Kanban Board | In GitLab, issue boards can easily be configured to support Kanban delivery practices.  The key is to pay close attention to the capacity of the team in each stage of the work and to leverage the [issue weights](https://docs.gitlab.com/ee/user/project/issue_board.html#sum-of-issue-weights) of all the issues in process at any point in time. |
| Backlog | tbd |


## SAFe
[SAFe](https://www.scaledagileframework.com/about/) is a Scaled Agile Framework provided by Scaled Agile Inc. It is an enterprise-level framework in which work moves through the portfolio, program and team in an orderly, hierarchical way, providing suggested metrics and best practices in each area.

## Jira integration
GitLab Enterprise edition can [integrate](https://about.gitlab.com/features/jira/) directly with Atlassian Jira. When using Jira for issue management, Jira remains the system of record for agile user story management. GitLab branch, commit and merge request data can then be shared back to the Jira Development Panel via the integration. Workflow automation can be triggered by including a Jira issue ID and state in a GitLab commit message, which then could transition a Jira issue to a closed state.


## Resources

* [GitLab for Agile](https://about.gitlab.com/2018/03/05/gitlab-for-agile-software-development/)
* [4 ways to use issue boards](https://about.gitlab.com/2018/08/02/4-ways-to-use-gitlab-issue-boards/)
* [GitLab project mgt youtube](https://www.youtube.com/watch%3Fv%3DLB5zvfjIDi0&ved=2ahUKEwjw2cGf5-rdAhUkWN8KHeE8BJAQjjgwA3oECAUQAQ&usg=AOvVaw1yAMfhvN3pjNciSoNUmDjQ)
* [GitLab, Jira and Jenkins youtube](https://www.youtube.com/watch%3Fv%3DJn-_fyra7xQ&ved=2ahUKEwjw2cGf5-rdAhUkWN8KHeE8BJAQjjgwE3oECAEQAQ&usg=AOvVaw3jCWmPspuIWQqz0YfMwRG1)
* [GitLab and atlassian](https://www.youtube.com/watch%3Fv%3Do7pnh9tY5LY&ved=2ahUKEwjgrJ-I6OrdAhWpc98KHSRMCSwQjjgwBHoECAgQAQ&usg=AOvVaw0fHEQPLR8-oJnaweIwXQdo)
