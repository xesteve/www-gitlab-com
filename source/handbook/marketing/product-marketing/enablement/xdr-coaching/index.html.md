---
layout: markdown_page
title: "XDR Enablement"
---



## XDR Enablement Topics


###  Enterprise IT & their challenges 

**Learning Objectives / Key points / What we're going to test:**
1. Digital Transformation - IT expected to enable faster delivery of customer led innovation.
2. Continuously deliver high quality end user experience - quality with speed.
3. Excessive complexity across IT architecture and infrastructure.
4. Lack of full metrics and visibility across the entire process.
5. Compliance - prove compliance with IT controls and industry regulations.
6. Security - IT teams are under pressure to reduce risk, prevent and mitigate leaks.
7. Culture and collaboration issues in the workplace - legacy organizations and silos.
8. Budget constraints limit their options.


**Related Reading:**
* [10 digital transformation success stories](https://www.cio.com/article/3149977/digital-transformation/digital-transformation-examples.html?nsdr=true#tk.cio_rs)
* [The CIO’s Dilemma: Innovate AND Cut Costs](https://www.cio.com/article/3300871/cloud-computing/the-cio-s-dilemma-innovate-and-cut-costs.html)
* [Complexity a killer when it comes to digital transformation success](https://www.cio.com/article/3269493/digital-transformation/complexity-a-killer-when-it-comes-to-digital-transformation-success.html)
* [5 Big Challenges CIOs face](https://www.mrc-productivity.com/blog/2017/11/5-big-challenges-facing-cios-leaders-2018/)
* [9 forces shaping the future of IT](https://www.cio.com/article/3206770/it-strategy/9-forces-shaping-the-future-of-it.html?upd=1538513299753)
* [12 biggest issues IT faces](https://www.cio.com/article/3245772/it-strategy/the-12-biggest-issues-it-faces-today.html)
* [Survey: Compliance Drives IT Security](https://www.cio.com/article/2447696/compliance/survey--compliance-drives-it-security.html)
* [Financial Services Regulatory Compliance](https://about.gitlab.com/solutions/financial-services-regulatory-compliance/)
* [Collaboration key to achieving business goals](https://www.cio.com/article/3170784/collaboration/collaboration-key-to-achieving-business-goals.html)

### [Enterprise IT Roles](/handbook/marketing/product-marketing/enterprise-it-roles/) 

**Learning Objectives / Key points / What we're going to test:**

For CxO, VP, Director and Manager levels, understand their goals, focus, top pain points

**Pre-work**  Read and review these articles and pages:
* [IT Job Roles](https://targetpostgrad.com/subjects/computer-science-and-it/it-job-roles-and-responsibilities-explained)
* [IT OrgCharts and Roles](http://www.bmcsoftware.in/guides/itil-itsm-roles-responsibilities.html)
* [CIO Role](https://www.thebalancecareers.com/business-or-it-what-s-the-main-job-of-a-cio-2071252)
* [Enterprise IT Roles](../enterprise-it-roles)

### Qualification Questions   

**Role:** App development manager

* What they do

  * How frequently does your team ship code.  Are you trying to accelerate?
  * How much of their delivery toolchain is integrated  (Listen for their SCM) (Jira, GitHub, Jenkins, Gerrit, artifactory, ansible, Fortify, etc)
  * Who is responsible to keep the integrated tool chain working
  * What tech/language do they use and how big is their team

* What pain do they face

  * Is your team able to keep up with the business demand
  * What are the bottlenecks that slow down your team's ability to deliver?
  * Do you have to deal with issues in the toolchain?  (Jenkins upgrades, integrations breaking, adding new developers, removing people from the team.   What about compliance and audit?

* Resources/Pre-work (read these):

  * https://blog.hubspot.com/sales/16-sales-qualification-questions-to-identify-prospects-worth-pursuing
  * https://blog.close.io/42-b2b-qualifying-questions-to-ask-sales-prospects





