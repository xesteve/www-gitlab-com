---
layout: markdown_page
title: "Blueprints"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Overview

Blueprints are intended to flesh out our initial thinking about specific problems and issues
we are facing (topical) and outline overall Infrastructure priorities and focus for a given
quarter (quarterly). Blueprints are sketches whose purpose is to foster and frame discussion
around Infrastructure topics, most of which will yield [designs](/handbook/engineering/infrastructure/design/)
and [OKRs](/handbook/okrs/).

## Index

### Quarterly

Listed reversed-chronologically:

* [2018/Q4](2018-q4/)

### Topical

Listed alphabetically:

* [Deltas](deltas/)
* [Dogfooding CI/CD](ci-cd/)
* [Planning Workflow](planning/)
* [Service Levels and Error Budgets](service-levels-error-budgets/)
