---
layout: markdown_page
title: Support
description: "Visit the GitLab support page to find product support links and to contact the support team."
---

# GitLab Support
{:.no_toc}

GitLab offers a variety of support options for all customers and users, on both paid
and free tiers. You should be able to find help using the resources linked below, regardless
of how you use GitLab. There are many ways to [contact Support](#contact-support), but
the first step for most people should be to [search our documentation](#search-our-documentation).

If you can't find an answer to your question, or you are affected by an outage, then
customers who are in a **paid** tier should start by looking at:
* [Scope of Support for GitLab Self-managed Licenses (Starter, Premium, Ultimate)](scope-of-support-gitlab-self-managed.html)
* [Scope of Support for GitLab.com Paid Subscriptions (Bronze, Silver, Gold)](scope-of-support-gitlab-com.html)

If you're using one of GitLab's **free** options, please jump to the
[statement of support for free tier users](#Scope-of-Support-for-Free-Tier-Users).

Finally, the Support Team asks that you check what is [outside the scope of support](#Outside-of-the-Scope-of-Support-for-all-Tiers)
before you contact support.

Please understand that any support that might be offered beyond the scope defined here is done
at the discretion of the agent or engineer and is provided as a courtesy.

# Contact Support
{:.no_toc}

| **Self-managed (hosted on your own site/server)** |
| --------------------- |
|  [GitLab Community Forum](https://forum.gitlab.com)<br/>I'm using a self-managed GitLab Core or Community Edition server without a paid license, and I need help. |
|  [GitLab Customer Support](https://support.gitlab.com)<br/>I have a paid license (Starter, Premium, Ultimate), and I need help. |

| **GitLab.com** |
| --------------------- |
|  [GitLab Community Forum](https://forum.gitlab.com)<br/>I need help using GitLab.com. |
|  [GitLab.com Account Support](https://support.gitlab.com)<br/>I need help accessing my free GitLab.com account. |
|  [GitLab Customer Support](https://support.gitlab.com)<br/>I have a paid subscription to GitLab.com (Bronze, Silver, Gold), and I need help. <br/>A bug is severely impacting my ability to use GitLab.com. |

Additional resources for getting help, reporting issues, requesting features, and so forth are listed on our [get help page](/get-help/).



## GitLab Support Offerings
{:.no_toc}

- TOC
{:toc}

## GitLab Paid Tier Support Offerings
GitLab offers 24x5 support (24x7 for Premium Support Emergency tickets) bound by the SLA times listed below.
The SLA times listed are the time frames in which you can expect the first response and each successive response.
GitLab Support will make a best effort to resolve any issues to your satisfaction as quickly as possible. However, the
SLA times are *not* to be considered as an expected time-to-resolution.

### Scope of Support for Paid Tiers
* [Scope of Support for GitLab Self-managed Licenses (Starter, Premium, Ultimate)](scope-of-support-gitlab-self-managed.html)
* [Scope of Support for GitLab.com Paid Subscriptions (Bronze, Silver, Gold)](scope-of-support-gitlab-com.html)

### Premium Support

Premium Support is available for GitLab [Premium, Ultimate](/pricing/#self-hosted), [Gold, and Sliver](/pricing/#gitlab-com) plans.

If your organization purchased a plan with **Premium Support**, this includes:

#### Tiered Support responses

| Type            | SLA | Hours |  How to Submit |
|-----------------|---------|----|---------------|
| Production Application Unavailable (Emergency) | 30 minutes | 24x7 | When you receive your license file, you will also receive a set of email addresses to use to reach Support for both regular and emergency requests (separate addresses) |
| Highly Degraded | 4 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/) or via the regular support email. |
| Medium Impact   | 8 hrs | 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/) or via the regular support email. |
| Low Impact      | 24 hrs| 24x5 | Please submit requests through the [support web form](https://support.gitlab.com/) or via the regular support email. |


**24x7 emergency support (Not available for Gold Plans)**: GitLab responds within 30 minutes to any emergency.

**Support for High Availability (HA)**: A Support Engineer will work with your technical team around any issues encountered after an HA implementation is completed in cooperation with our Customer Success team.

**Live upgrade assistance**: Schedule an upgrade time with GitLab. We'll host a live screen share session to help you through the process
and ensure there aren't any surprises.

#### Definitions of Support Impact

- **Urgent** - GitLab is unavailable or completely unusable (Emergency)
  If a GitLab server or cluster is not available, or otherwise unusable, an emergency ticket can be filed and our On-Call Support Engineer will respond within 30 minutes. Example: GitLab showing 502 errors for all users.
- **High** - GitLab is Highly Degraded (4 hours)
  Significant Business Impact. Important GitLab features are unavailable, or extremely slowed, with no acceptable workaround. Implementation or production use of GitLab is continuing; however, there is a serious impact on productivity. Example: CI Builds are erroring and not completing successfully, and the software release process is significantly affected.
- **Normal** - There appears to be a bug preventing normal GitLab operation (12 hours)
  Some Business Impact. Important GitLab features are unavailable, or somewhat slowed, but a workaround is available. GitLab use has a minor loss of operational functionality, regardless of the environment or usage. Example: A Known bug impacts the use of GitLab, but a workaround is successfully being used as a temporary solution.
- **Low** - Questions or Clarifications around features or documentation or deployments (24 hours)
  Minimal Business Impact. Information, an enhancement, or documentation clarification is requested, but there is no impact on the operation of GitLab. Implementation or production use of GitLab is continuing and work is not impeded. Example: A question about enabling ElasticSearch.

#### Upgrading to Premium Support
If your organization would like to upgrade to a plan with Premium Support, you can
purchase it yourself [online](https://customers.gitlab.com), email your account manager
or email `renewals@gitlab.com`.

### Standard Support (Available for GitLab [Starter](/pricing/#self-hosted), and [Bronze plans](/pricing/#gitlab-com))

Subscribers with Standard Support receive next business day support via e-mail.

Please submit your support request through the [support web form](https://support.gitlab.com/).
When you receive your license file, you will also receive an email address to use to
reach Support in case the web form can't be reached for any reason.

### Support for GitHost

Subscribers to GitHost receive next business day support via e-mail.

Please submit your support request through the [support web form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334487).


# Scope of Support for Free Tier Users

## Self-managed (Core and Community Edition)

If you are seeking help with your GitLab Core or Community Edition installation, note that
the GitLab Support Team is unable to directly assist with issues with specific installations
of these versions. Please use the following resources instead:

* [GitLab Documentation](https://docs.gitlab.com): extensive documentation
  regarding the supported configurations of GitLab.
* [GitLab Community Forum](https://forum.gitlab.com/): this is the best place to have
  a discussion about your Community Edition configuration and options.
* [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): please search
  for similar issues before posting your own, there's a good chance somebody else
  had the same issue as you and has already found a solution.

Our [community advocates](/handbook/marketing/developer-relations/community-advocacy/)
also spend time on the Community Forum and Stack Overflow to help where they can, and
escalate issues as needed.


## GitLab.com Users (Free / Early Adopter Plans)

Technical and general support for those using our free options is “Community First”.
Like many other free SaaS products, users are first directed to find support in community
sources such as our own [Community Forum](https://forum.gitlab.com),
[Stackoverflow](http://stackoverflow.com/questions/tagged/gitlab), Google, etc. You
can also follow [GitLabStatus on Twitter](https://twitter.com/GitLabStatus) for status
updates for the GitLab.com site, or check [https://status.gitlab.com/](https://status.gitlab.com/) to see if
there is a known service outage.

The GitLab team _does_ offer support for:
  - account specific issues (lost password, GDPR, etc.)
  - broken features/states for specific users or repositories
  - issues with GitLab.com availability

For help with these issues please open a
[support request](https://support.gitlab.com).
For *free* GitLab.com users seeking support, a support agent or engineer may determine that the request
is more appropriate for community forums than for official support.

Note that issues affecting paid users receive a higher priority. There are no SLAs
or guaranteed response times associated with free accounts.

### GitLab.com Specific Support Policies

#### Account Recovery
If you have lost access to your account, perhaps due to having lost access to your
2FA device or the original email address that the account was set up with, the account
*may* be recovered provided the claimant can provide sufficient evidence of account
ownership.  Use the [support web form](https://support.gitlab.com/) to request assitance.

Please note that in some cases reclaiming an account may be impossible. Read
["How to keep your GitLab account safe"](/2018/08/09/keeping-your-account-safe/)
for advice on preventing this.

#### Dormant Username Requests
The GitLab.com Support Team will consider an account to be "dormant" when the user has not logged in or otherwise used the account over an extended time. Namespaces associated with dormant accounts can be reassigned if both of the following are true:

1. The user's last sign in was at least two years ago.
2. The user is not the sole owner of any active projects.

If the account contains data, GitLab Support will attempt to contact the user over a two week period before reassigning the username. If the account contains no data, the dormant username will be released immediately.

Usernames and namespaces associated with unconfirmed accounts over 90 days old will also be released immediately.

## Outside of the Scope of Support for all Tiers

### Git
1. `git` specific commands and issues (not related to GitLab)

### CI/CD
1. Helping debug specific commands or scripts in a `.gitlab-ci.yml`
1. Issues other than configuration or setup of private runner **hosts**


## General Support Practices

#### Issue Creation
When bugs, regressions, or any application behaviors/actions *not working as intended*
are reported or discovered during support interactions, the GitLab Support Team will
create issues in GitLab project repos on behalf of our customers.

For feature requests, both involving addition of new features as well as change of
features currently *working as intended*, GitLab support will request that the customer
create the issue on their own in the appropriate project repos.

#### Closing Tickets
If a customer explains that they are satisfied their concern is being addressed
properly in an issue created on their behalf, then the conversation should continue
within the issue itself, and GitLab support will close the support ticket. Should
a customer wish to reopen a support ticket, they can simply reply to it and it will
automatically be reopened.


## Further resources

Additional resources for getting help, reporting issues, requesting features, and
so forth are listed on our [get help page](/get-help/).