stages:

  # Stages are defined in the product categories handbook https://about.gitlab.com/handbook/product/categories/#hierarchy

  manage:
    display_name: "Manage"
    image: "/images/solutions/solutions-plan.png"
    description: "Gain visibility and insight into how your business is performing."
    body: |
          GitLab helps teams manage and optimize their software delivery lifecycle with metrics and value stream insight in order to streamline and increase their delivery velocity.   Learn more about how GitLab helps to manage your end to end <a href="https://about.gitlab.com/vsm">value stream.</a>
    vision: /direction/manage
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amanage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    related:
      - "plan"
      - "create"
      - "verify"
      - "package"
      - "release"
      - "configure"
      - "monitor"
      - "secure"
    dev_ops: dev
    pm: Jeremy Watson
    pmm: John Jeremiah
    cm: Suri Patel
    engineering_manager: Liam McAndrew
    frontend_engineering_manager: Tim Z (Interim)
    ux: Chris
    stage: true

  plan:
    display_name: "Plan"
    image: "/images/solutions/solutions-plan.png"
    description: "Regardless of your process, GitLab provides powerful planning
      tools to keep everyone synchronized."
    body: |
      GitLab enables portfolio planning and management through epics, groups (programs) and milestones to organize and track progress.  Regardless of your methodology from Waterfall to DevOps, GitLab’s simple and flexible approach to planning meets the needs of small teams to large enterprises.

      GitLab helps teams organize, plan, align and track project work to ensure teams are working on the right things at the right time and maintain end to end visibility and traceability of issues throughout the delivery lifecycle from idea to production.

    vision: /direction/plan
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aplan&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2011
    related:
      - "create"
    dev_ops: dev
    pm: Victor Wu
    pmm: John Jeremiah
    cm: Suri Patel
    engineering_manager: Sean McGivern
    frontend_engineering_manager: André Luís
    ux: Pedro Moreira da Silva
    stage: true

  create:
    display_name: "Create"
    image: "/images/solutions/solutions-create.png"
    description: "Create, view, and manage code and project data through powerful branching tools."
    body: |
      GitLab helps teams design, develop and securely manage code and project data from a single distributed version control system to enable rapid iteration and delivery of business value.  GitLab repositories provide a scalable, single source of truth for collaborating on projects and code which enables teams to be productive without disrupting their workflows.

    vision: /direction/create
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Acreate&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2011
    related:
      - "plan"
      - "verify"
    dev_ops: dev
    pm: James Ramsay
    pmm: John Jeremiah
    cm: Suri Patel
    engineering_manager: Douwe Maan
    frontend_engineering_manager: André Luís
    ux: Jeethu
    stage: true

  # distribution:
  #   display_name: "Distribution"
  #   image: "/images/solutions/solutions-distribution.png"
  #   description: "Add distribution description here"
  #   body: |
  #     GitLab helps distribution
  #
  #   vision: /direction/distribution
  #   roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
  #   established: 2012
  #   related:
  #     - "create"
  #   dev_ops: dev
  #   pm: Joshua Lambert
  #   pmm: William Chia
  #   engineering_manager: Marin Jankovski
  #   frontend_engineering_manager: Clement Ho
  #   ux: Dimitrie Hoekstra
  #   stage: true
  #   internal_customers:
  #     - Quality Department

  # gitaly_and_gitter:
  #   display_name: "Gitaly and Gitter"
  #   image: "/images/solutions/solutions-gitaly.png"
  #   description: "Add Gitaly and Gitter description here"
  #   body: |
  #     GitLab makes Gitaly and Gitter
  #
  #   vision: /direction/gitaly
  #   roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
  #   established: 2012
  #   related:
  #     - "create"
  #   dev_ops: dev
  #   pm: James
  #   pmm: John
  #   engineering_manager: Tommy (interim)
  #   frontend_engineering_manager: Tim Z (Interim)
  #   ux: Dimitrie Hoekstra
  #   stage: true
  #   internal_customers:
  #     - Quality Department

  # geo:
  #   display_name: "Geo"
  #   image: "/images/solutions/solutions-geo.png"
  #   description: "Add Geo description here"
  #   body: |
  #     GitLab makes Geo
  #
  #   vision: /direction/geo
  #   roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Adistribution&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
  #   established: 2012
  #   related:
  #     - "create"
  #   dev_ops: dev
  #   pm: Andreas
  #   pmm: John
  #   engineering_manager: Rachel Nienaber
  #   frontend_engineering_manager: André Luís (Interim)
  #   ux: Dimitrie Hoekstra
  #   stage: true
  #   internal_customers:
  #     - Quality Department

  verify:
    display_name: "Verify"
    image: "/images/solutions/solutions-verify.png"
    description: "Keep strict quality standards for production code with automatic testing and reporting."
    body: |
      GitLab helps delivery teams fully embrace continuous integration to automate the builds, integration and verification of their code.  GitLab’s industry leading CI capabilities enables automated testing, Static Analysis Security Testing, Dynamic Analysis Security testing and code quality analysis to provide fast feedback to developers and testers about the quality of their code.  With pipelines that enable concurrent testing and parallel execution, teams quickly get insight about every commit, allowing them to deliver higher quality code faster.

    vision: /direction/verify
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name[]=devops%3Averify&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2012
    related:
      - "create"
    dev_ops: ops
    pm: Jason Lenny (Interim)
    pmm: William Chia
    cm: Aricka Flowers
    engineering_manager: Elliot Rushton
    frontend_engineering_manager: Tim Z (Interim)
    ux: Dimitrie Hoekstra
    stage: true
    internal_customers:
      - Quality Department

  package:
    display_name: "Package"
    image: "/images/solutions/solutions-package.png"
    description: "Deploy quickly at massive scale with integrated Docker Container Registry"
    body: |
      GitLab helps teams package their applications and manage their containers with a secure and private container registry for their Docker images. The GitLab Container Registry isn't just a standalone registry; it's completely integrated with GitLab. You can now easily use your images for GitLab CI, create images specific for tags or branches and much more.

      Our container registry is fully integrated with Git repository management and comes out of the box with GitLab his means our integrated Container Registry requires no additional installation. It allows for easy upload and download of images from GitLab CI.

    vision: /direction/package
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Apackage&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    dev_ops: ops
    pm: Joshua Lambert
    pmm: William Chia
    cm: Aricka Flowers
    engineering_manager: Marin Jankovski (Interim)
    frontend_engineering_manager: Clement Ho
    ux: Hazel
    stage: true
    internal_customers:
      - Distribution Team

  release:
    display_name: "Release"
    image: "/images/solutions/solutions-release.png"
    description: "GitLab's integrated CI/CD allows you to ship code quickly, be it on one -or one thousand servers."
    body: |
      GitLab helps teams automate the release and delivery of their applications to enable them to shorten the delivery lifecycle, streamline manual processes and accelerate team velocity.  With Continuous Delivery (CD), built into the pipeline, deployment can be automated to multiple environments like staging and production, and support advanced features such as canary deployments.   Because the configuration and definition of the application is version controlled and managed, it is easy to configure and deploy your application on demand.

    vision: /direction/release
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Arelease&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    related:
      - "configure"
    dev_ops: ops
    pm: Jason Lenny
    pmm: William Chia
    cm: Aricka Flowers
    engineering_manager: Elliot Rushton (Interim)
    frontend_engineering_manager: Tim Z (Interim)
    ux: Dimitrie
    stage: true
    internal_customers:
      - Release Managers

  configure:
    display_name: "Configure"
    image: "/images/solutions/solutions-configure.png"
    description: "Configure your applications and infrastructure."
    body: "GitLab helps teams to configure and manage their application environments.  Strong integrations to Kubernetes simplifies the effort to define and configure the infrastructure needed to support your application.   Protect access to key infrastructure configuration details such as passwords and login information by using ‘secret variables’ to limit access to only authorized users and processes."

    vision: /direction/configure
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Aconfigure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2018
    related:
      - "configure"
    dev_ops: ops
    pm: Daniel Gruesso
    pmm: William Chia
    cm: Aricka Flowers
    engineering_manager: Dylan Griffith
    frontend_engineering_manager: Tim Z (Interim)
    ux: Taurie
    stage: true
    internal_customers:
      - Site Availability Engineering
      - Site Reliability Engineering

  monitor:
    display_name: "Monitor"
    image: "/images/solutions/solutions-monitor.png"
    description: "Automatically monitor metrics so you know how any change in code
      impacts your production environment."
    body: |
      You need feedback on what the effect of your release is in order to do
      release management. Get monitoring built-in, see the code change
      right next to the impact that it has so you can respond quicker and
      effectively. No need to babysit deployment to manually get feedback.
      Automatically detect buggy code and prevent it from affecting the
      majority of your users.

    vision: /direction/monitor
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Amonitor&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2016
    related:
      - "release"
    dev_ops: ops
    pm: Joshua Lambert
    pmm: William Chia
    cm: Aricka Flowers
    engineering_manager: Seth Engelhard
    frontend_engineering_manager: Clement Ho
    ux: Amelia
    stage: true
    internal_customers:
      - Infrastructure Department
  secure:
    display_name: "Secure"
    image: "/images/solutions/solutions-secure.png"
    description: "Security capabilities, integrated into your development lifecycle."
    body: |
      GitLab provides Static Application Security Testing (SAST), Dynamic Application Security Testing (DAST), Container Scanning, and Dependency Scanning to help you deliver secure applications.

    vision: /direction/secure
    roadmap: "https://gitlab.com/groups/gitlab-org/-/roadmap?label_name%5B%5D=devops%3Asecure&scope=all&sort=end_date_asc&state=opened&layout=QUARTERS"
    established: 2017
    related:
      - "create"
    dev_ops: ops
    pm: Fabio Busatto
    pmm: Cindy Blake
    cm: Erica Lindberg
    engineering_manager: Philippe Lafoucrière
    frontend_engineering_manager: Tim Z (Interim)
    ux: Andy
    stage: true
    internal_customers:
      - Security Department
